


</td>
</tr>
</tbody>
</table>
<footer class="ap-footer">
  <div class="footer-main">
    <div class="text-center">
    <button class="btn btn-default btn-footer-custom collapsed" style="color:#fff;background-color:#7c3531;border-color:#fff" type="button" data-toggle="collapse" data-target="#footerCollapse" aria-expanded="false" aria-controls="footerCollapse" onclick="openNAV()" style="z-index:w: 10">
      網站地圖
    </button>
    </div>
    <div class="footer-table__top collapse" id="footerCollapse" >
      <div class="ap-table table-fixed rwd">
        <div class="ap-table-row">
          <div class="ap-table-cell cell-20 align-top">
            <div class="top__title">關於我們</div>
            <a href="/about-us.php" class="top__link">噯仙堂本草</a>
            <a href="/stores.php" class="top__link">門市據點</a>
          </div>
          <div class="ap-table-cell cell-20 align-top">
            <div class="top__title">產品系列</div>
            <?php foreach ($teabags as $tb) { ?>
                <a href="<?= $tb['link'] ?>" class="top__link"><?= $tb['name'] ?></a>
            <?php } ?>
          </div>
          <div class="ap-table-cell cell-20 align-top">
            <div class="top__title">客戶服務</div>
            <a href="/sop.php" class="top__link">訂購流程</a>
            <a href="/faq.php" class="top__link">常見問答</a>
          </div>
          <div class="ap-table-cell cell-20 align-top">
            <div class="top__title">網路商城</div>
            <a href="https://aixian1894.qdm.tw/" class="top__link">噯仙堂網路商店</a>
              <a href="https://shopee.tw/jasmineh09" class="top__link">噯仙堂蝦皮商店</a>
              <a href="https://www.rakuten.com.tw/shop/aixian1894" class="top__link">噯仙堂樂天市場</a>
          </div>
        </div>
      </div>
    </div>
<div class="footer">
<div class="container" style="padding-top:0em;">
<div class="row">
<div class="col-lg-5 col-xs-12 about-company">
<strong><font size="6">噯仙堂本草</font></strong>
<p class="pr-5 text-white-50">為您健康把關，為您身體打氣，時時擁有健康好氣息。 </p>

<p><a href="https://www.facebook.com/214939325227846" target="_blank"><img src="img/icons8-facebook-96.png" width=48 style="vertical-align: bottom;"></a> <a href="https://www.instagram.com/aixianspring1894/" target="_blank"><i class="fa fa-instagram fa-3x fa-fw" aria-hidden="true" style="color:#dd2a7b"></i></a> <a href="http://nav.cx/iQ0Kbit" target="_blank"><img src="img/icons8-line-96.png" width=48 style="vertical-align: bottom;"></a></p>
</div>
<div class="col-lg-3 col-xs-12 links">

</div>
<div class="col-lg-4 col-xs-12 location">
<h4 class="mt-lg-0 mt-sm-4">總公司地址</h4>
<p>330 桃園市桃園區大仁路43號</p>
<p class="mb-0"><i class="fa fa-phone mr-3"></i>(03)367-8881</p>
<p class="mb-0"><i class="fa fa-phone mr-3"></i>(03)367-8882(傳真)</p>
<p><i class="fa fa-envelope-o mr-3"></i>hct1894@gmail.com</p>
</div>
</div>

</div>
</div>
  </div>
</footer>  

<div id="footer">
  <div id="footer-content"><div class="wsite-elements wsite-footer">
    <div class="paragraph" style="text-align:center;"><span>噯仙堂本草©</span>Aixian1894</div></div></div>
  </div>
</div>
</div></div></div>
</div>
</div>  
</div>  
</div>  
</div>
</div>
</div>  
</div>

        <div class="navmobile-wrapper" style="padding-top: 5px;">
          <div id="navmobile" class="nav">
            <div class="wsite-mobile-menu" style="display: block;">
              <div class="wsite-animation-wrap" style="position: relative;height: 100%;">
                <ul class="wsite-menu-default wsite-menu-slide" style="position: absolute; top: 0px; left: 0px; width: 100%; height: 100%;">
                <?php foreach ($head_navs as $i => $nav) { ?>
                <li id="<?php if(0==$i) { echo 'active'; }?>" class="wsite-menu-item-wrap">
                <a href="<?= $nav['url'] ?>" class="wsite-menu-item">
                  <?= $nav['text'] ?>
                </a>
                </li>
                <?php } ?>
                </ul>
              </div>
            </div>
          </div>
        </div>
</div>
</body>
</html>
<script>
    // 來源底圖網址
    var url_nlsc = "https://wmts.nlsc.gov.tw/wmts/{Layer_ID}/default/GoogleMapsCompatible/{z}/{y}/{x}";

    // 來源底圖定義
    var map_EMAP = L.tileLayer(url_nlsc,
        {
            attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
            maxZoom: 20,
            Layer_ID: 'EMAP'
        });

    // 設置地圖(包括中心點跟放大縮小的級距等等 中心點亦可定義為取得的經緯度)
    var mymap = L.map('mapid', {
        zoomControl: false,
        zoomDelta: 0.5,
        zoomSnap: 0
    }).setView([24.9727264, 121.3232357], 14);

    // 地圖控制部份
    L.control.scale({ 'position': 'bottomleft', 'metric': true, 'imperial': false }).addTo(mymap);

    //套入來源底圖
    mymap.addLayer(map_EMAP);
    
    //放入經緯度 先緯度再經度
    var marker = L.marker([24.9727264, 121.3232357]).addTo(mymap)

    //slick setting
    $(document).ready(function(){
        $('.carousel').carousel({
            interval: 6000
        })

        $(".store_list_item_show_details").click(function(e) {
          var longtitude = $(this).data('x');
          var latitude = $(this).data('y');
          var name = $(this).data('name');
          
            mymap.removeLayer(marker);
            
            marker = L.marker([latitude, longtitude]).addTo(mymap)
                .bindPopup(name)
                .openPopup();
            mymap.setView([latitude, longtitude], 14);    
        
        });

        $('.btn-primary-lg').click(function(e){
          location.href = $(this).data('link');
	});

    });

</script>
<script src="js/slick.min.js"></script>
<script src="https://unpkg.com/axios/dist/axios.min.js"></script>
<script>
$('.slick').slick({
dots: true,             //顯示輪播圖片會顯示圓圈
infinite: true,         //重覆輪播
slidesToShow:1,         //輪播顯示個數
slidesToScroll: 1,      //輪播捲動個數
autoplay: true,         //autoplay : 自動播放
responsive: [

{
breakpoint: 1000,
settings: {
slidesToShow: 1,
slidesToScroll: 1,
arrows:false
}
},
{
breakpoint: 480,
settings: {
slidesToShow: 1,
slidesToScroll: 1,
arrows:false
}
}]
});

</script>
