<html lang="zh_TW">
<head>
    <title>噯仙堂本草</title>
<meta property="og:site_name" content="Boutir">
<meta property="og:url" content="https://www.aixian1894.com/">
<meta name="author" content="Itsour Hsu">
<meta name="title" content="噯仙堂本草">
<meta name="description" content="「噯仙堂」創始於西元一八九四年，由第一代創始人徐噯 中醫師 所創立。他是日據時代在台灣桃園第一個取得中醫師執照的台灣人。徐醫生懸壺濟世，救人無數，代代相傳至今已第五代，超過百年傳承。  噯仙堂本草擁有百年五代家傳的本草中藥經驗，以漢方本草為基底，結合現代生物食品科技的技術，研發出沖泡式漢方草本茶包，讓您養生更輕鬆方便。所有產品皆嚴選藥材研製而成，完全不使用食品添加劑及調味料，100%純天然，讓您喝得更安心。  噯仙堂以漢方草本文化為主軸，傳達輕鬆養生概念 – 原來養生也可以這麼容易，輕鬆享受健康! 擁抱健康!  香港 DLA International Company Limited 現為台灣知名品牌-噯仙堂本草在香港及大中華地區的總代理商。此網站只提供噯仙堂中藥材及食品的零售銷售，如果您對噯仙堂商品有興趣進行批發，歡迎您隨時與我們預約商談">
<meta property="og:url" content="https://www.aixian1894.com/">
<meta property="og:title" content="噯仙堂本草">
<meta property="og:description" content="「噯仙堂」創始於西元一八九四年，由第一代創始人徐噯 中醫師 所創立。他是日據時代在台灣桃園第一個取得中醫師執照的台灣人。徐醫生懸壺濟世，救人無數，代代相傳至今已第五代，超過百年傳承。  噯仙堂本草擁有百年五代家傳的本草中藥經驗，以漢方本草為基底，結合現代生物食品科技的技術，研發出沖泡式漢方草本茶包，讓您養生更輕鬆方便。所有產品皆嚴選藥材研製而成，完全不使用食品添加劑及調味料，100%純天然，讓您喝得更安心。  噯仙堂以漢方草本文化為主軸，傳達輕鬆養生概念 – 原來養生也可以這麼容易，輕鬆享受健康! 擁抱健康!  香港 DLA International Company Limited 現為台灣知名品牌-噯仙堂本草在香港及大中華地區的總代理商。此網站只提供噯仙堂中藥材及食品的零售銷售，如果您對噯仙堂商品有興趣進行批發，歡迎您隨時與我們預約商談">
<meta name="keywords" content="文創 噯仙堂 中藥 噯仙堂本草 eshop igshop facebookshop m-commerce sitebuilder shopping shopline shopify instagram 網上商店">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="refresh" content="3; url=contact.php"> 
<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="css/main.css">
<link rel="stylesheet" type="text/css" href="css/sites.css">
<link rel="stylesheet" type="text/css" href="css/slick.css"/>
<link rel="stylesheet" type="text/css" href="css/slick-theme.css"/>
<script src="js/jquery.min.js"></script>
<script>
window.jQuery || document.write('<script src="http://34.211.140.79/js/jquery.min.js"><\/script>');
</script>
<script language="javascript" src="js/plugin.js"></script>
<script language="javascript" src="js/custom.js"></script>
<script language="javascript" src="js/mobile.js"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
    <!-- 引入地圖套件的CSS跟JS -->
    <link rel="stylesheet" href="https://unpkg.com/leaflet@1.3.4/dist/leaflet.css" integrity="sha512-puBpdR0798OZvTTbP4A8Ix/l+A4dHDD0DGqYW6RQ+9jxkRFclaxxQb/SJAWZfWAkuyeQUytO7+7N4QKrDh+drA=="
        crossorigin="" />
    <script src="https://unpkg.com/leaflet@1.3.4/dist/leaflet.js" integrity="sha512-nMMmRyTVoLYqjP9hrbed9S+FzjZHW5gY1TWCHA5ckwXZBadntCNs8kEqAWdrb9O7rxbCaA4lKTIWjDXZxflOcA=="
        crossorigin=""></script>
</head>
<?php include("config.php");?>
