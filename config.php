<?php
$head_navs = [
 ['id' => 'index', 'url' => "/", 'text' => "首頁"],
 ['id' => 'pg554594452700954385', 'url' => "/about-us.php", 'text' => "關於我們"],
 ['id' => 'pg953319480201285323', 'url' => "/product-category.php", 'text' => "產品介紹"],
 ['id' => 'pg256118286107654007', 'url' => "/stores.php", 'text' => "門市據點"],
 ['id' => 'pg799130269578270611', 'url' => "https://aixian1894.qdm.tw/contact", 'text' => "聯繫我們"],
 ['id' => 'pg468421220198319485', 'url' => "https://aixian1894.qdm.tw/", 'text' => "網路商店"],
 ['id' => 'pg468421220198319487', 'url' => "https://shopee.tw/jasmineh09", 'text' => "蝦皮商店"],
 ['id' => 'pg468421220198319489', 'url' => "https://www.rakuten.com.tw/shop/aixian1894", 'text' => "樂天市場"],
];
$products = [
    ['link' => 'https://aixian1894.qdm.tw/product/category&path=2', 'name' => '頂級漢方藥膳(燉煮式)', 'name_en' => 'Medicinal diet', 'src' => '/img/cat_soup.jpg'],
    ['link' => 'https://aixian1894.qdm.tw/product/category&path=4', 'name' => '頂級漢方茶飲(燉煮式)', 'name_en' => 'Drink', 'src' => '/img/cat_drink.jpg'],
    ['link' => 'https://aixian1894.qdm.tw/%E6%BC%A2%E6%96%B9%E6%9C%89%E6%A9%9F%E9%A3%9F%E9%A4%8A', 'name' => '漢方有機食養', 'name_en' => 'Organic Food', 'src' => '/img/cat_organic.jpg'],
    ['link' => '#', 'name' => '頂級漢方草本茶(沖泡式)', 'name_en' => 'Tea', 'src' => '/img/cat_teabag.jpg'],
    ['link' => 'https://aixian1894.qdm.tw/%E8%AA%BF%E5%91%B3%E7%B2%89', 'name' => '家傳香料', 'name_en' => 'Spices', 'src' => '/img/cat_powder_0605.jpg'],
    ['link' => 'https://aixian1894.qdm.tw/product/category&path=3', 'name' => '漢方橄欖', 'name_en' => 'Olive', 'src' => '/img/cat_olive.jpg'],
    ['link' => 'https://aixian1894.qdm.tw/%E6%9C%89%E6%A9%9F%E9%A3%9F%E5%93%81', 'name' => '有機食品/雜糧', 'name_en' => 'Grains', 'src' => '/img/cat_organic0311.jpg'],
    ['link' => 'https://aixian1894.qdm.tw/%E9%A0%82%E7%B4%9A%E9%A3%9F%E6%9D%90', 'name' => '頂級食材/乾貨', 'name_en' => 'Top Notic', 'src' => '/img/cat_organic0512.jpg'],
    ['link' => 'https://aixian1894.qdm.tw/%E5%88%9D%E6%9E%9C%E6%9E%9C%E5%8F%B0%E7%81%A3%E5%8F%A4%E6%97%A9%E5%91%B3%E7%B3%BB%E5%88%97/%E5%8F%B0%E7%81%A3%E5%8F%A4%E6%97%A9%E5%91%B3%E7%B3%BB%E5%88%97', 'name' => '初果果/台灣古早味系列', 'name_en' => 'Sugar', 'src' => '/img/cat_kudamono.jpg'],
];

$teabags = [
    ['name' => '基礎養生系列', 'link' => 'https://aixian1894.qdm.tw/%E5%9F%BA%E7%A4%8E%E9%A4%8A%E7%94%9F%E7%B3%BB%E5%88%97'],
    ['name' => '氣色紅潤系列', 'link' => 'https://aixian1894.qdm.tw/%E6%B0%A3%E8%89%B2%E7%B4%85%E6%BD%A4%E7%B3%BB%E5%88%97'],
    ['name' => '纖體美妍系列', 'link' => 'https://aixian1894.qdm.tw/%E7%BA%96%E9%AB%94%E7%BE%8E%E5%A6%8D%E7%B3%BB%E5%88%97'],
    ['name' => '軟Q系列', 'link' => 'https://aixian1894.qdm.tw/%E8%BB%9FQ%E7%B3%BB%E5%88%97'],
    ['name' => '好精神系列', 'link' => 'https://aixian1894.qdm.tw/%E5%A5%BD%E7%B2%BE%E7%A5%9E%E7%B3%BB%E5%88%97'],
    ['name' => '其他養生系列', 'link' => 'https://aixian1894.qdm.tw/%E5%85%B6%E4%BB%96%E9%A4%8A%E7%94%9F%E7%B3%BB%E5%88%97'],
];
