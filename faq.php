<?php
include('header.php');
?>
<body class="no-header  wsite-page-index  full-width-on  wsite-theme-light postload menu-open"><div class="body-wrap">

	<div id="header">
		<div class="nav-trigger hamburger">
			<div class="open-btn">
				<span class="mobile"></span>
				<span class="mobile"></span>
				<span class="mobile"></span>
			</div>
		</div>
		<div id="sitename"><span class="wsite-logo">

	<a href="/">
          <img src="/img/aixian_logo.png" width="200" alt="噯仙堂本草logo">
	</a>

</span><br>
台灣漢方有機食養頂級品牌<br><br></div>
	</div>

	<div id="wrapper">
	  <div class="bg-wrapper">
          <?php include("menu.php") ?>
	  <div id="content-wrapper">
	    <div id="wsite-content" class="wsite-elements wsite-not-footer">
	      <div class="wsite-section-wrap">
	        <div class="wsite-section wsite-body-section wsite-background-18 wsite-custom-background">
		  <div class="wsite-section-content">
		    <div class="container">
                     <div class="wsite-section-elements">
<div style="height: 40px; overflow: hidden; width: 100%;"></div></div>

<h2 class="wsite-content-title" style="text-align:left;">
<span style="">常見問答</span><br></h2>
<div><div style="height: 20px; overflow: hidden;"></div>
<div class="column-left">
		<p class="faq_q">Q1. 產品需要冷藏或冷凍嗎?</p>
		<p class="faq_ans">  產品外包裝上皆有清楚標示產品保存方式，常溫保存/冷藏/冷凍，請按照產品說明。</p>
    <p class="faq_q">Q2. 上次跟這次買的同樣產品，為什麼吃起來口感不一樣?</p>
		<p class="faq_ans">  無論是中藥或草藥，或是農產品，因為源自天然環境，受採收時間，季節氣候等天然因素影響，口感、顏色或是軟硬度上難免會有些微不同。</p>
    <p class="faq_q">Q3. 我買的藥膳包，明明還沒有過期，為什麼有小蟲子?</p>
		<p class="faq_ans">  中草藥產品都是源自天然環境，經過曬乾或烘乾後，仍有可能發生蟲蛀及發霉狀況，需密封保存並至於通風陰涼處，開封後，建議冷藏。</p>
		<p class="faq_q">Q4. 如果想要大量訂購產品，請問會有折扣或優惠嗎?</p>
		<p class="faq_ans">  歡迎來電或來信詢問，我們有專業客服為您服務。</p>
</div>
<div class="column-right"></div>
<span style="display: block; clear: both; height: 0px; overflow: hidden;"></span>

<div style="height: 20px; overflow: hidden;"></div></div>
<?php require('footer.php'); ?>
