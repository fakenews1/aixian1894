<?php
include('header.php');
?>
<body class="no-header  wsite-page-index  full-width-on  wsite-theme-light postload menu-open"><div class="body-wrap">

	<div id="header">
		<div class="nav-trigger hamburger">
			<div class="open-btn">
				<span class="mobile"></span>
				<span class="mobile"></span>
				<span class="mobile"></span>
			</div>
		</div>
		<div id="sitename"><span class="wsite-logo">

	<a href="/">
          <img src="/img/aixian_logo.png" width="200" alt="噯仙堂本草logo">
	</a>

</span><br>
台灣漢方有機食養頂級品牌<br><br></div>
	</div>

	<div id="wrapper">
	  <div class="bg-wrapper">
          <?php include("menu.php") ?>
	  <div id="content-wrapper">
	    <div id="wsite-content" class="wsite-elements wsite-not-footer">
	      <div class="wsite-section-wrap">
	        <div class="wsite-section wsite-body-section wsite-background-18 wsite-custom-background">
		  <div class="wsite-section-content">
		    <div class="container">
<h2 class="wsite-content-title" style="text-align:left;">
<span style="">訂購流程</span><br></h2>
<div><div style="height: 20px; overflow: hidden;"></div>
<div class="column-left">
    <h2>國內直購流程:</h2>
    <br>
    <p>(1) 選擇以下任一訂購方式:</p>
    <br>
    <p>※電話訂購 客服電話: 03-367-8881 或 03-365-8885</p>
	<p>※傳真訂購 客服傳真: 03-367-8882</p>
	<p>※信件或 Line訊息至客服 </p>
        <div style="padding-left:2rem"> 
	客服信箱: hct1894@gmail.com<br>
	   Line+好友搜尋:@aixain1894<br><br>
        </div>  
    <p>(2) 填寫產品收件資料(姓名/電話/地址)</p> 
    <p>(3) 確認您的訂單總金額(含訂單金額及運費)</p>
    <p>(4) 匯款完成，2-3個工作天出貨</p>
    <p>(5) 訂單金額(折扣後)滿$1500元即享免運費
    訂單金額未滿$1500元，需自負運費$100元</p>

	<h3>付款方式:</h3>
	<br>
    <p>※ ATM 轉帳</p>
    <p>※ 轉帳匯款</p>
    <p>※ 貨到付款(購物滿$1500元可貨到付款)</p>

    <br><br>

    <h2>跨國訂購流程:</h2>
    <br>
    <p>(1) 選擇以下任一訂購方式:</p>
    <br>
    <p>※電話訂購 客服電話: 03-367-8881 或 03-365-8885</p>
	<p>※傳真訂購 客服傳真: 03-367-8882</p>
	<p>※信件或 Line訊息至客服 </p>
        <div style="padding-left:2rem"> 
	客服信箱: hct1894@gmail.com<br>
	Line+好友搜尋:@aixain1894<br><br>
        </div>  
    <p>(2) 填寫產品收件資料(姓名/電話/地址)</p> 
    <p>(3) 確認您的訂單總金額(含訂單金額及運費)</p>
    <p>(4) 寄送線上結帳單完成付款</p> 
    <p>(5) 匯款完成，2-3個工作天出貨</p>

    <h3>付款方式:</h3><br>
    <p>※ATM轉帳</p>
    <p>※轉帳匯款-提供台幣帳戶</p>
    <p>※信用卡付款-使用線上結帳單</p>
    <br><br>

    <h2>網路商店訂購:</h2><br>
    <p>噯仙堂網路商店</p><br>
    <p>網址: <a href="https://aixian1894.qdm.tw/">https://aixian1894.qdm.tw/</a></p><br><br>
</div>
<span style="display: block; clear: both; height: 0px; overflow: hidden;"></span>

<div style="height: 20px; overflow: hidden;"></div></div>
<?php require('footer.php'); ?>
