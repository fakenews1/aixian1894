<?php
include('header.php');
function mxp_get_instagram_photos($username) {
    if ($username == "") {
        return array();
    }
    $url = "https://www.instagram.com/" . $username;
    $ch  = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.13) Gecko/20080311 Firefox/2.0.0.13');
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE);
    $output = curl_exec($ch);
    curl_close($ch);
  
    $data_images = array();
    $data        = explode('window._sharedData = ', $output);
    $data_json   = explode(';</script>', $data[1]);
    $data_json   = json_decode($data_json[0], TRUE);
    if (!empty($data_json['entry_data']['ProfilePage'][0]['graphql']['user']['edge_owner_to_timeline_media']['edges'])) {
        foreach ($data_json['entry_data']['ProfilePage'][0]['graphql']['user']['edge_owner_to_timeline_media']['edges'] as $image) {
            $data_images[] = array(
                'id'      => $image['node']['id'],
                'time'    => $image['node']['taken_at_timestamp'],
                'like'    => $image['node']['edge_liked_by']['count'],
                'comment' => $image['node']['edge_media_to_comment']['count'],
                'caption' => !empty($image['node']['edge_media_to_caption']['edges'][0]['node']['text']) ? $image['node']['edge_media_to_caption']['edges'][0]['node']['text'] : '',
                'link'    => rtrim('//instagram.com/p/' . $image['node']['shortcode'], '/\\') . '/',
                'images'  => array(
                    'display'   => $image['node']['display_url'],
                    'thumbnail' => $image['node']['thumbnail_src'],
                ),
            );
        }
    }
    if (0 == count($data_images)) {
        $output = file_get_contents('ig_0611.json');
        $data_json = json_decode($output, true);
        if (!empty($data_json['graphql']['user']['edge_owner_to_timeline_media']['edges'])) {
            foreach ($data_json['graphql']['user']['edge_owner_to_timeline_media']['edges'] as $image) {
                $data_images[] = array(
                    'id'      => $image['node']['id'],
                    'time'    => $image['node']['taken_at_timestamp'],
                    'like'    => $image['node']['edge_liked_by']['count'],
                    'comment' => $image['node']['edge_media_to_comment']['count'],
                    'caption' => !empty($image['node']['edge_media_to_caption']['edges'][0]['node']['text']) ? $image['node']['edge_media_to_caption']['edges'][0]['node']['text'] : '',
                    'link'    => rtrim('//instagram.com/p/' . $image['node']['shortcode'], '/\\') . '/',
                    'images'  => array(
                        'display'   => $image['node']['display_url'],
                        'thumbnail' => $image['node']['thumbnail_src'],
                    ),
                );
            }
        }
    }
    return $data_images;
}
?>
<body class="no-header  wsite-page-index  full-width-on  wsite-theme-light postload menu-open"><div class="body-wrap">

	<div id="header">
		<div class="nav-trigger hamburger">
			<div class="open-btn">
				<span class="mobile"></span>
				<span class="mobile"></span>
				<span class="mobile"></span>
			</div>
		</div>
		<div id="sitename"><span class="wsite-logo">

	<a href="/">
          <img src="/img/aixian_logo.png" width="200" alt="噯仙堂本草logo">
	</a>

</span><br>
台灣漢方有機食養頂級品牌<br><br>
</div>
	</div>

	<div id="wrapper">
	  <div class="bg-wrapper">
          <?php include("menu.php"); ?>
	  <div id="content-wrapper">
	    <div id="wsite-content" class="wsite-elements wsite-not-footer">
	      <div class="wsite-section-wrap">
	        <div class="wsite-section wsite-body-section wsite-background-18 wsite-custom-background">
		  <div class="wsite-section-content">
		    <div class="container">
			<div class="wsite-section-elements">
				<div><div id="868150177859877753" align="left" style="width: 100%; overflow-y: hidden;" class="wcustomhtml"><meta name="google-site-verification" content="S5PtT9Gtnkm9DVGTulq8NAgv9ipibFIMt4eGzGrlTHQ"></div>



</div>
                <div class="slick">
                    <a href="https://aixian1894.qdm.tw/%E5%88%9D%E6%9E%9C%E6%9E%9C%E5%8F%B0%E7%81%A3%E5%8F%A4%E6%97%A9%E5%91%B3%E7%B3%BB%E5%88%97/%E5%8F%B0%E7%81%A3%E5%8F%A4%E6%97%A9%E5%91%B3%E7%B3%BB%E5%88%97" target="_blank"><img src="img/main-16.jpg" width=100% alt="台灣古早味系列"></a>
                    <a href="https://aixian1894.qdm.tw/%E5%9F%BA%E7%A4%8E%E9%A4%8A%E7%94%9F%E7%B3%BB%E5%88%97" target="_blank"><img src="img/main-14.jpg" width=100% alt="基礎養生系列"></a>
                    <a href="https://aixian1894.qdm.tw/%E6%B0%A3%E8%89%B2%E7%B4%85%E6%BD%A4%E7%B3%BB%E5%88%97" target="_blank"><img src="img/main-15.jpg" width=100% alt="氣色紅潤系列"></a>
                    <a href="https://aixian1894.qdm.tw/%E6%9C%89%E6%A9%9F%E9%A3%9F%E5%93%81" target=_blank"><img src="img/main-11.jpg" width=100% alt="有機食品"></a>
                    <a href="https://aixian1894.qdm.tw/頂級食材" target="_blank"><img src="img/main-10.jpg" width=100% alt="頂級食材"></a>
                    <a href="https://aixian1894.qdm.tw/product/category&path=2" target="_blank"><img src="img/main-02.jpg" width=100% alt="湯品"></a>
                    <a href="https://aixian1894.qdm.tw/調味粉" target="_blank"><img src="img/main-03.jpg" width=100% alt="調味粉"></a>
                    <a href="https://aixian1894.qdm.tw/product/category&path=3" target=_blank"><img src="img/main-09.jpg" width=100% alt="橄欖"></a>
                    <a href="https://aixian1894.qdm.tw/product/category&path=4" target="_blank"><img src="img/main-01.jpg" width=100% alt="飲品"></a>
                    <a href="https://aixian1894.qdm.tw/漢方有機食養" target="_blank"><img src="img/main-12.jpg" width=100% alt="漢方有機食養"></a>
                </div>

<div><div style="height: 40px; overflow: hidden; width: 100%;"></div>
<hr class="styled-hr" style="width:100%;">
<div style="height: 40px; overflow: hidden; width: 100%;"></div></div>
                <?php
                $photos = mxp_get_instagram_photos('aixianspring1894');
                ?>
<h2 class="wsite-content-title" style="text-align:left;"><span style="">照片集錦</span><br></h2>
<div><div style="height: 20px; overflow: hidden;"></div>
<div id="146558494816021328-gallery" class="gallery" style="line-height: 0px; padding: 0; margin: 0">
  <?php require("images.php"); ?>

  <?php foreach ($photos as $i => $image) { ?>
  <div id="146558494816021328-imageContainer<?= $i ?>" style="float:left;width:24.95%;margin:0;">
    <div id="146558494816021328-insideImageContainer<?= $i ?>" style="position:relative;margin:5px;">
      <div class="galleryImageHolder" style="position:relative; width:100%; padding:0 0;overflow:hidden;">
<?php 
        $lines = explode("\n", $image['caption']);
		$url = $image['link'];
                foreach ($lines as $word){
                  if ( (strpos($word, "http") != -1) || (strpos($word, "www.") === 0) ){
			    $match = preg_match_all("/http(s)?:\/\/.+/", $word, $arr);
			  if ($match) { 
		  	    $url = $arr[0][0]; 
	                  }
                  }
                }
?>
      <div class="galleryInnerImageHolder"><a href="<?= $url ?>" rel="lightbox[gallery146558494816021328]" class="w-fancybox" target="_blank">
	<img src="/img/ig-0<?= ($i+1)?>.png" class="galleryImage" style="float:left;line-height:10px;padding:5px;"></a>
        </div>
      </div>
    </div>
  </div>
      <?php if ($i >= 7) { break; } ?>
  <?php } ?>

<span style="display: block; clear: both; height: 40px; overflow: hidden;"></span>
</div>
<?php /*
<h2 class="wsite-content-title" style="text-align:left;"><span style="">頂級漢方草本茶 - 纖體美妍系列</span><br></h2>
<div><div style="height: 20px; overflow: hidden;"></div>
     <div id="146558494816021328-gallery" class="gallery" style="line-height: 0px; padding: 0; margin: 0">

        <?php foreach ($imageContainers2 as $i => $image) { ?>
        <div id="146558494816021328-imageContainer<?= $i ?>" style="float:left;width:24.95%;margin:0;">
            <div id="146558494816021328-insideImageContainer<?= $i ?>" style="position:relative;margin:5px;">
                <div class="galleryImageHolder" style="position:relative; width:100%; padding:0 0;overflow:hidden;">
                    <div class="galleryInnerImageHolder"><a href="<?= $image['url'] ?>" rel="lightbox[gallery146558494816021328]" class="w-fancybox">
                        <img src="<?= $image['url'] ?>" class="galleryImage" style="float:left;line-height:10px;padding:5px;"></a>
                    </div>
                </div>
            </div>
        </div>
        <?php } ?>

        <span style="display: block; clear: both; height: 40px; overflow: hidden;"></span>
     </div>
     <div style="height: 20px; overflow: hidden;"></div>
</div>
    <h2 class="wsite-content-title" style="text-align:left;"><span style="">頂級漢方草本茶 - 軟Q系列</span><br></h2>
    <div><div style="height: 20px; overflow: hidden;"></div>
        <div id="146558494816021328-gallery" class="gallery" style="line-height: 0px; padding: 0; margin: 0">
            <?php foreach ($imageContainers3 as $i => $image) { ?>
                <div id="146558494816021328-imageContainer<?= $i ?>" style="float:left;width:24.95%;margin:0;">
                    <div id="146558494816021328-insideImageContainer<?= $i ?>" style="position:relative;margin:5px;">
                        <div class="galleryImageHolder" style="position:relative; width:100%; padding:0 0;overflow:hidden;">
                            <div class="galleryInnerImageHolder"><a href="<?= $image['url'] ?>" rel="lightbox[gallery146558494816021328]" class="w-fancybox">
                                    <img src="<?= $image['url'] ?>" class="galleryImage" style="float:left;line-height:10px;padding:5px;"></a>
                            </div>
                        </div>
                    </div>
                </div>
            <?php } ?>

            <span style="display: block; clear: both; height: 40px; overflow: hidden;"></span>
        </div>
        <div style="height: 20px; overflow: hidden;"></div>
    </div>

    <h2 class="wsite-content-title" style="text-align:left;"><span style="">頂級漢方草本茶 - 氣色紅潤系列</span><br></h2>
    <div><div style="height: 20px; overflow: hidden;"></div>
        <div id="146558494816021328-gallery" class="gallery" style="line-height: 0px; padding: 0; margin: 0">
            <?php foreach ($imageContainers4 as $i => $image) { ?>
                <div id="146558494816021328-imageContainer<?= $i ?>" style="float:left;width:20%;margin:0;">
                    <div id="146558494816021328-insideImageContainer<?= $i ?>" style="position:relative;margin:5px;">
                        <div class="galleryImageHolder" style="position:relative; width:100%; padding:0 0;overflow:hidden;">
                            <div class="galleryInnerImageHolder"><a href="<?= $image['url'] ?>" rel="lightbox[gallery146558494816021328]" class="w-fancybox">
                                    <img src="<?= $image['url'] ?>" class="galleryImage" style="float:left;line-height:10px;padding:5px;"></a>
                            </div>
                        </div>
                    </div>
                </div>
            <?php } ?>

            <span style="display: block; clear: both; height: 40px; overflow: hidden;"></span>
        </div>
        <div style="height: 20px; overflow: hidden;"></div>
    </div>

    <h2 class="wsite-content-title" style="text-align:left;"><span style="">頂級漢方草本茶 - 好精神系列</span><br></h2>
    <div><div style="height: 20px; overflow: hidden;"></div>
        <div id="146558494816021328-gallery" class="gallery" style="line-height: 0px; padding: 0; margin: 0">
            <?php foreach ($imageContainers5 as $i => $image) { ?>
                <div id="146558494816021328-imageContainer<?= $i ?>" style="float:left;width:20%;margin:0;">
                    <div id="146558494816021328-insideImageContainer<?= $i ?>" style="position:relative;margin:5px;">
                        <div class="galleryImageHolder" style="position:relative; width:100%; padding:0 0;overflow:hidden;">
                            <div class="galleryInnerImageHolder"><a href="<?= $image['url'] ?>" rel="lightbox[gallery146558494816021328]" class="w-fancybox">
                                    <img src="<?= $image['url'] ?>" class="galleryImage" style="float:left;line-height:10px;padding:5px;"></a>
                            </div>
                        </div>
                    </div>
                </div>
            <?php } ?>

            <span style="display: block; clear: both; height: 40px; overflow: hidden;"></span>
        </div>
        <div style="height: 20px; overflow: hidden;"></div>
    </div>

    <h2 class="wsite-content-title" style="text-align:left;"><span style="">頂級漢方草本茶 - 其他系列</span><br></h2>
    <div><div style="height: 20px; overflow: hidden;"></div>
        <div id="146558494816021328-gallery" class="gallery" style="line-height: 0px; padding: 0; margin: 0">
            <?php require("images.php"); ?>
            <?php foreach ($imageContainers6 as $i => $image) { ?>
                <div id="146558494816021328-imageContainer<?= $i ?>" style="float:left;width:24.95%;margin:0;">
                    <div id="146558494816021328-insideImageContainer<?= $i ?>" style="position:relative;margin:5px;">
                        <div class="galleryImageHolder" style="position:relative; width:100%; padding:0 0;overflow:hidden;">
                            <div class="galleryInnerImageHolder"><a href="<?= $image['url'] ?>" rel="lightbox[gallery146558494816021328]" class="w-fancybox">
                                    <img src="<?= $image['url'] ?>" class="galleryImage" style="float:left;line-height:10px;padding:5px;"></a>
                            </div>
                        </div>
                    </div>
                </div>
            <?php } ?>

            <span style="display: block; clear: both; height: 0px; overflow: hidden;"></span>
        </div>
    </div>
    */ ?>
<?php require('footer.php'); ?>
