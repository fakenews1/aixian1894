<?php
include('header.php');
?>
<body class="no-header  wsite-page-index  full-width-on  wsite-theme-light postload menu-open"><div class="body-wrap">

	<div id="header">
		<div class="nav-trigger hamburger">
			<div class="open-btn">
				<span class="mobile"></span>
				<span class="mobile"></span>
				<span class="mobile"></span>
			</div>
		</div>
		<div id="sitename"><span class="wsite-logo">

	<a href="/">
          <img src="/img/aixian_logo.png" width="200" alt="噯仙堂本草logo">
	</a>

</span><br>
台灣漢方有機食養頂級品牌<br><br></div>
	</div>

	<div id="wrapper">
	  <div class="bg-wrapper">
          <?php include("menu.php") ?>
	  <div id="content-wrapper">
	    <div id="wsite-content" class="wsite-elements wsite-not-footer">
	      <div class="wsite-section-wrap">
	        <div class="wsite-section wsite-body-section wsite-background-18 wsite-custom-background">
		  <div class="wsite-section-content">
		    <div class="container">

<div><div class="wsite-multicol"><div class="wsite-multicol-table-wrap" style="margin:0 -20px;">
	<table class="wsite-multicol-table">
		<tbody class="wsite-multicol-tbody">
			<?php foreach ($products as $product) { ?>
			<tr class="wsite-multicol-tr">
				<td class="wsite-multicol-col" style="width:60%; padding:0 0px;">
					<div><div class="wsite-image wsite-image-border-border-width:0 " style="padding-top:0px;padding-bottom:15px;margin-left:0px;margin-right:0px;text-align:left">
						<a href="#"> 	
							<img src="<?= $product['src'] ?>" alt="Picture" style="width:672;max-width:100%">
						</a>
						<div style="display:block;font-size:90%"></div>
					</div></div>		
				</td>
				<td class="wsite-multicol-col" style="width:29.38731596829%; padding:40px 10px;">
					<h2 class="wsite-content-title" style="text-align:left;"><font size="6"><?= $product['name'] ?></font></h2>
						<div class="paragraph" style="text-align:left;"><font size="5"><font size="4"><?= $product['name_en'] ?></font></font><br></div>
						<div style="text-align:left;"><div style="height: 10px; overflow: hidden;"></div>
                        <?php if ('Tea' == $product['name_en']) { ?>
                            <?php foreach ($teabags as $tb) { ?>
                                <a href="<?= $tb['link'] ?>"><span class="teabag-btn"><?= $tb['name'] ?></span></a>
                            <?php } ?>
                        <?php } else { ?>
						    <a href="<?= $product['link'] ?>"><span class="aixian-btn">更多詳情</span></a>
                        <?php } ?>
						<div style="height: 10px; overflow: hidden;"></div>
				</td>
			</tr>
			<?php } ?>
		</tbody>
	</table>
</div></div></div>
<?php require('footer.php'); ?>
