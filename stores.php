<?php
if(!isset($place)) $place = '北部';
include('header.php');
?>
<body class="no-header  wsite-page-index  full-width-on  wsite-theme-light postload menu-open"><div class="body-wrap">

	<div id="header">
		<div class="nav-trigger hamburger">
			<div class="open-btn">
				<span class="mobile"></span>
				<span class="mobile"></span>
				<span class="mobile"></span>
			</div>
		</div>
		<div id="sitename"><span class="wsite-logo">

	<a href="/">
          <img src="/img/aixian_logo.png" width="200" alt="噯仙堂本草logo">
	</a>

</span><br>
台灣漢方有機食養頂級品牌<br><br></div>
	</div>

	<div id="wrapper">
	  <div class="bg-wrapper">
          <?php include("menu.php") ?>
	  <div id="content-wrapper">
	    <div id="wsite-content" class="wsite-elements wsite-not-footer">
	      <div class="wsite-section-wrap">
	        <div class="wsite-section wsite-body-section wsite-background-18 wsite-custom-background">
		  <div class="wsite-section-content">
		    <div class="container">
                     <div class="wsite-section-elements">
<div style="height: 40px; overflow: hidden; width: 100%;"></div></div>

<h2 id="store-info" class="wsite-content-title" style="text-align:left;">
<span style="">門市資訊</span><br></h2>
<div><div style="height: 20px; overflow: hidden;"></div>
<div id="store-menu" class="store-menu">
	<ul class="categories_list">
		<li>
<label class="btn-primary-lg <?= ('北部' == $place) ? 'active' : '' ?>" data-link="stores.php#store-info">北部</label>
</li>
<li>
<label class="btn-primary-lg <?= ('中部' == $place) ? 'active' : '' ?>" data-link="store_c.php#store-info">中部</label>
</li>
<li>
<label class="btn-primary-lg <?= ('南部' == $place) ? 'active' : '' ?>" data-link="store_s.php#store-info">南部</label>
</li>
</div>
<div id="store_locator_content" class="clearfix">
	<div id="store_list" class="store_list">
	    <div class="store_list_item" style="display:none">
            <div class="store_list_item_name">總公司門市</div>
            <div class="store_list_item_add_info">
                <div class="store_list_item_distance"></div>
                <div class="store_list_item_address">地址：桃園市桃園區大仁路43號</div>
                <div class="store_phone">
                	<a href="callto:03 367-8881">電話：03-367-8881</a><br>
                	傳真：03-367-8882<br>
                	<a href="callto:03 365-8885">電話：客服：03-365-8885</a><br>
                </div>
                <div class="store_list_item_email">E-mail: spring1894@yahoo.com.tw</div>
                <div class="store_list_item_fb">Facebook粉絲專頁：回春堂本草</div>
                <span class="store_list_item_show_details" data-y="24.9727264" data-x="121.3232357" data-name="總公司門市">詳情</span>
            </div>
        </div>
        <?php require('store_config.php');
        foreach ($stores[$place] as $type => $store_items) {
        ?>
        	<div class="store_list_item">
        		    <div class='item-type'><?= $type ?></div>
        	</div>
        	<?php foreach ($store_items as $item) { ?>
            <div class="store_list_item">
              <div class="store_list_item_name"><?= $item['name'] ?></div>
              <div class="store_list_item_add_info">
                <div class="store_list_item_distance"></div>
                <span class="store_list_item_show_details" data-y="<?= $item['longtitude'] ?>" data-x="<?= $item['latitude'] ?>" data-name="<?= $item['name'] ?>">櫃點詳情</span>
              </div>
            </div>  
              <?php 
            }
        } ?>
    </div>     
    <div id="mapid" class="store_map" style="position: relative; overflow: hidden;"></div>
</div>

<span style="display: block; clear: both; height: 0px; overflow: hidden;"></span>

<div style="height: 20px; overflow: hidden;"></div></div>
<?php require('footer.php'); ?>
