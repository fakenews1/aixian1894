<?php

$imageContainers = [
    ['url' => 'img/health01.jpg'],
    ['url' => 'img/health02.jpg'],
    ['url' => 'img/health03.jpg'],
    ['url' => 'img/health04.jpg'],
    ['url' => 'img/health05.jpg'],
    ['url' => 'img/health06.jpg'],
];
$imageContainers2 = [
    ['url' => 'img/beauty01.jpg'],
    ['url' => 'img/beauty02.jpg'],
    ['url' => 'img/beauty03.jpg'],
];
$imageContainers3 = [
    ['url' => 'img/niku01.jpg'],
    ['url' => 'img/niku02.jpg'],
];

$imageContainers4 = [
    ['url' => 'img/aka01.jpg'],
    ['url' => 'img/aka02.jpg'],
    ['url' => 'img/aka03.jpg'],
    ['url' => 'img/aka04.jpg'],
    ['url' => 'img/aka05.jpg'],
];

$imageContainers5 = [
    ['url' => 'img/genki01.jpg'],
    ['url' => 'img/genki02.jpg'],
    ['url' => 'img/genki03.jpg'],
    ['url' => 'img/genki04.jpg'],
    ['url' => 'img/genki05.jpg'],
];

$imageContainers6 = [
    ['url' => 'img/other01.jpg'],
    ['url' => 'img/other02.jpg'],
    ['url' => 'img/other03.jpg'],
    ['url' => 'img/other04.jpg'],
    ['url' => 'img/other05.jpg'],
    ['url' => 'img/other06.jpg'],
    ['url' => 'img/other07.jpg'],
    ['url' => 'img/other08.jpg'],
];