<div id="navigation">
  <ul class="wsite-menu-default">
    <?php foreach ($head_navs as $i => $nav) { ?>
      <li id="<?= $nav['id']; if(0==$i) { echo 'active'; }?>" class="wsite-menu-item-wrap wsite-nav-<?= ++$i ?>" style="position: relative;">
        <a href="<?= $nav['url'] ?>" class="wsite-menu-item" style="position: relative;">
          <?= $nav['text'] ?>
        </a>
      </li>
    <?php } ?>
  </ul>
</div>
